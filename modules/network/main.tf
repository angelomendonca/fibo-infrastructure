resource "aws_vpc" "vpc" {
  cidr_block       = "192.0.128.0/21"
  instance_tenancy = "dedicated"

  tags {
    Name = "main"
  }
}

# the bastion host is in the public subnet
resource "aws_subnet" "public_subnet_1a" {

  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "192.0.133.0/24"
  availability_zone = "ap-southeast-2a"

  tags {
    Name = "public_subnet_1a"
  }
}


# the webserver is in a private subnet - app
resource "aws_subnet" "private_subnet_1c" {
  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "192.0.129.0/24"
  availability_zone = "ap-southeast-2c"

  tags {
    Name = "private_subnet_1c"
  }
  depends_on = ["aws_vpc.vpc"]
}

# the db needs this
# you need atleast 2 subnets each in a diff AZ. - db
resource "aws_subnet" "private_subnet_1b" {
  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "192.0.132.0/24"
  availability_zone = "ap-southeast-2b"

  tags {
    Name = "private_subnet_1b"
  }
}


# two public subnets, one in each availability zone
# https://aws.amazon.com/premiumsupport/knowledge-center/public-load-balancer-private-ec2/
# these are needed by the load balancer - app
resource "aws_subnet" "public_subnet_1b" {

  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "192.0.130.0/24"
  availability_zone = "ap-southeast-2b"

  tags {
    Name = "public_subnet_1b"
  }
}

resource "aws_subnet" "public_subnet_1c" {

  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "192.0.131.0/24"
  availability_zone = "ap-southeast-2c"

  tags {
    Name = "public_subnet_1c"
  }
}

# ----------
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "iot-test-vpc-gateway"
  }
}

resource "aws_route_table" "public_route_table" {
  vpc_id = "${aws_vpc.vpc.id}"
}


resource "aws_route" "route" {

  route_table_id            = "${aws_route_table.public_route_table.id}"
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                = "${aws_internet_gateway.gw.id}"
}


resource "aws_route_table_association" "subnet_route_table" {

  subnet_id      = "${aws_subnet.public_subnet_1a.id}"
  route_table_id = "${aws_route_table.public_route_table.id}"
}


resource "aws_route_table_association" "subnet_route_table_1b" {

  subnet_id      = "${aws_subnet.public_subnet_1b.id}"
  route_table_id = "${aws_route_table.public_route_table.id}"
}


resource "aws_route_table_association" "subnet_route_table_1c" {

  subnet_id      = "${aws_subnet.public_subnet_1c.id}"
  route_table_id = "${aws_route_table.public_route_table.id}"
}