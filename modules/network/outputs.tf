output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "default_security_group_id" {
  value = "${aws_vpc.vpc.default_security_group_id}"
}

output "private_subnet_1c" {
  value = "${aws_subnet.private_subnet_1c.id}"
}


output "private_subnet_1b" {
  value = "${aws_subnet.private_subnet_1b.id}"
}

output "public_subnet_1a" {
  value = "${aws_subnet.public_subnet_1a.id}"
}

output "public_subnet_1b" {
  value = "${aws_subnet.public_subnet_1b.id}"
}

output "public_subnet_1c" {
  value = "${aws_subnet.public_subnet_1c.id}"
}
