#!/bin/bash

if [ -f /etc/system-release ]; then
	sudo yum -y update
	sudo sed -i -e '/\[epel\]/,/^\[/s/enabled=0/enabled=1/' /etc/yum.repos.d/epel.repo
	sudo yum install -y software-properties-common ansible git python-boto
else
	sudo apt-get -y update
    sudo apt-get install -y software-properties-common
    sudo apt-add-repository -y ppa:ansible/ansible
	sudo apt-get -y update
	sudo apt-get install -y git python-boto ansible
	if [ -f /tmp/config.ini ]; then
		sudo mkdir /app
		sudo chmod -R 775 /app
		sudo mv /tmp/config.ini /app/config.ini
	fi
fi

if [ ! -d ~/.ssh ] ; then
	mkdir ~/.ssh; echo -e "Host bitbucket.org\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config
else
	echo -e "Host bitbucket.org\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config
fi

chmod 400 ~/.ssh/config
FOLDER="/tmp/ansible-fibo-app"
REPO_URL="git@bitbucket.org:angelomendonca/fibo-deploy.git"

if [ ! -d "$FOLDER" ] ; then
    git clone "$REPO_URL" "$FOLDER"
else
    cd "$FOLDER"
    git pull
fi
ansible-playbook /tmp/ansible-fibo-app/playbooks/fibo-app/site.yml

