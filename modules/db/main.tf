resource "aws_db_subnet_group" "postgres_db" {
  name       = "main"
  subnet_ids = ["${var.private_subnet_1b}", "${var.private_subnet_1c}"]

  tags {
    Name = "Postgres"
  }
}

resource "aws_db_parameter_group" "rds-postgres" {
  name   = "rds-pg"
  family = "postgres9.6"

  parameter {
    name  = "client_encoding"
    value = "UTF8"
  }
}

resource "aws_security_group" "db_port" {
  name        = "db_port"
  description = "db port"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    security_groups  = ["${var.default_sg_group}"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_db_instance" "rds_pgsql" {
  allocated_storage    = 10
  storage_type         = "gp2"
  identifier           = "fibo-app"
  skip_final_snapshot   = true
  engine               = "postgres"
  engine_version       = "9.6.5"
  instance_class       = "db.m4.xlarge"
  name                 = "${var.database_name}"
  username             = "${var.database_user}"
  password             = "${var.database_password}"
  db_subnet_group_name = "${aws_db_subnet_group.postgres_db.name}"
  parameter_group_name = "${aws_db_parameter_group.rds-postgres.name}"
  vpc_security_group_ids   = ["${aws_security_group.db_port.id}"]
}

# replace with your domain name. Just a placeholder.
resource "aws_route53_zone" "test" {
  name = "test.XXXXX.com"
  # vpc_id = "${var.vpc_id}" # will make it a private zone

  tags {
    Environment = "test"
  }
}

resource "aws_route53_record" "test_db" {
  zone_id = "${aws_route53_zone.test.zone_id}"
  name    = "fibodb"
  type    = "CNAME"
  ttl     = "300"
  records = ["${aws_db_instance.rds_pgsql.address}"]
}


