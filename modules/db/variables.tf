variable "vpc_id" {}
variable "private_subnet_1c" {}
variable "private_subnet_1b" {}
variable "default_sg_group" {}

variable "database_name" {}
variable "database_user" {}
variable "database_password" {}