variable "aws_region" {}
variable "vpc_id" {}
variable "default_sg_group" {}
variable "key_pair" {}
variable "bastion_host_ami" {}
variable "public_subnet_1a" {}

