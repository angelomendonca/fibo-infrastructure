resource "aws_instance" "jumphost" {

  ami           = "${var.bastion_host_ami}"
  instance_type = "m4.xlarge"

  availability_zone = "ap-southeast-2a"
  source_dest_check = false
  subnet_id         = "${var.public_subnet_1a}"
  key_name          = "${var.key_pair}"
  vpc_security_group_ids = ["${aws_security_group.ssh_22.id}", "${var.default_sg_group}"]
  iam_instance_profile = "${aws_iam_instance_profile.non-prod-profile.name}"
  tags {
    Role = "Jumphost"
    Env = "Test"
  }
  depends_on = ["aws_iam_instance_profile.non-prod-profile"]

}


resource "aws_security_group" "ssh_22" {

  name        = "SSH_To_Bastion_Host"
  description = "Allow SSH connections to jumphost"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_eip" "elastic_ip" {
  instance = "${aws_instance.jumphost.id}"
  vpc      = true
  # make sure your key is added to the agent or set it here.
  provisioner "remote-exec" {
    script = "./modules/bootstrap_scripts/bootstrap.sh"


    connection {
      type     = "ssh"
      user     = "ec2-user"
      agent    = "true"
      host     = "${aws_eip.elastic_ip.public_ip}"
      timeout  = "5m"
    }
    
  }

}


resource "aws_iam_policy" "get_infra_info" {

  name        = "get_infra_info"
  path        = "/"
  description = "Get Information about infrastructure"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "non-prod-role" {

  name = "non-prod-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = "${aws_iam_role.non-prod-role.name}"
  policy_arn = "${aws_iam_policy.get_infra_info.arn}"
}

resource "aws_iam_instance_profile" "non-prod-profile" {
  name  = "nonprod_profile"
  role = "${aws_iam_role.non-prod-role.name}"
}

