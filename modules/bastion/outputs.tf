output "jumphost_id" {
  value = "${aws_instance.jumphost.id}"
}

output "iam_instance_profile" {
  value = "${aws_iam_instance_profile.non-prod-profile.name}"
}

output "bastion_public_ip" {
  value = "${aws_eip.elastic_ip.public_ip}"
}