variable "aws_region" {}
variable "vpc_id" {}
variable "default_sg_group" {}
variable "key_pair" {}

variable "jumphost_id" {}
variable "iam_instance_profile" {}
variable "bastion_public_ip" {}
variable "rds_dns_name" {}
variable "app_server_ami" {}


variable "public_subnet_1b" {}
variable "public_subnet_1c" {}
variable "private_subnet_1c" {}
variable "private_subnet_1b" {}
