
resource "aws_route_table" "private_route_table" {
  vpc_id = "${var.vpc_id}"
}

# Provides a resource to create a routing table entry (a route) in a VPC routing table.
resource "aws_route" "private_route" {
  route_table_id            = "${aws_route_table.private_route_table.id}"
  destination_cidr_block    = "0.0.0.0/0"
  instance_id               = "${var.jumphost_id}"
}

resource "aws_route_table_association" "private_subnet_route_table_association" {
  subnet_id      = "${var.private_subnet_1c}"
  route_table_id = "${aws_route_table.private_route_table.id}"
}

resource "aws_route_table_association" "private_subnet_1b_route_table_association" {
  subnet_id      = "${var.private_subnet_1b}"
  route_table_id = "${aws_route_table.private_route_table.id}"
}

resource "aws_security_group" "ssh_internal_to_vpc" {
  name        = "ssh_internal_to_vpc"
  description = "ssh internally from any host who has the default group"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups  = ["${var.default_sg_group}"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}


resource "aws_security_group" "app_port" {
  name        = "app_port"
  description = "application port"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups  = ["${aws_security_group.elb_port.id}"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

data "template_file" "config" {
  template = "${file("${path.module}/config.tpl")}"

  vars {
    DB_HOSTNAME = "${var.rds_dns_name}"
  }
}


resource "aws_instance" "app_server" {
  ami           = "${var.app_server_ami}"
  instance_type = "m4.xlarge"
  availability_zone = "ap-southeast-2c"
  subnet_id         = "${var.private_subnet_1c}"
  key_name          = "${var.key_pair}"
  vpc_security_group_ids = ["${aws_security_group.app_port.id}", "${var.default_sg_group}",]

  iam_instance_profile = "${var.iam_instance_profile}"
  tags {
    Role = "app"
    Env = "Test"
  }
  depends_on = ["aws_route_table_association.private_subnet_route_table_association"]


      provisioner "file" {
          content      = "${data.template_file.config.rendered}"
          destination = "/tmp/config.ini"

          connection {
            type     = "ssh"
            user     = "ubuntu"
            agent    = "true"
            host     = "${aws_instance.app_server.private_ip}"
            timeout  = "10m"
            bastion_host = "${var.bastion_public_ip}"
            bastion_user = "ec2-user"
          }
      }

      provisioner "remote-exec" {
        script = "./modules/bootstrap_scripts/bootstrap.sh"

        connection {
          type     = "ssh"
          user     = "ubuntu"
          agent    = "true"
          host     = "${aws_instance.app_server.private_ip}"
          timeout  = "10m"
          bastion_host = "${var.bastion_public_ip}"
          bastion_user = "ec2-user"
        }
      }

}

resource "aws_security_group" "elb_port" {
  name        = "elb_port"
  description = "Elb Port"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}


resource "aws_elb" "fibo_elb" {
  name    = "fibo-terraform-elb"
  subnets = ["${var.public_subnet_1b}", "${var.public_subnet_1c}"]

  listener {
    instance_port     = 80
    instance_protocol = "HTTP"
    lb_port           = 80
    lb_protocol       = "HTTP"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }

  instances                   = ["${aws_instance.app_server.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags {
    Name = "fibo-terraform-elb"
  }
  security_groups = ["${aws_security_group.elb_port.id}"]

  depends_on = ["aws_instance.app_server"]
}
