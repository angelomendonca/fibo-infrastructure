variable "aws_region" {}
variable "key_pair" {}
variable "app_server_ami" {}
variable "bastion_host_ami" {}

variable "database_name" {}
variable "database_user" {}
variable "database_password" {}
variable "access_key" {}
variable "secret_key" {}


provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.aws_region}"
}


module "network" {
  source = "./modules/network"
}


module "bastion" {
  source = "./modules/bastion"

  aws_region = "${var.aws_region}"
  key_pair = "${var.key_pair}"
  bastion_host_ami ="${var.bastion_host_ami}"

  vpc_id = "${module.network.vpc_id}"
  default_sg_group = "${module.network.default_security_group_id}"
  public_subnet_1a = "${module.network.public_subnet_1a}"
}

module "db" {
  source = "./modules/db"

  private_subnet_1c = "${module.network.private_subnet_1c}"
  private_subnet_1b = "${module.network.private_subnet_1b}"

  vpc_id = "${module.network.vpc_id}"
  default_sg_group = "${module.network.default_security_group_id}"

  database_name= "${var.database_name}"
  database_user= "${var.database_user}"
  database_password= "${var.database_password}"

}

module "app" {
  source = "./modules/app"

  aws_region = "${var.aws_region}"
  key_pair = "${var.key_pair}"

  vpc_id = "${module.network.vpc_id}"
  default_sg_group = "${module.network.default_security_group_id}"

  # bastion modules outputs that are needed by app
  jumphost_id = "${module.bastion.jumphost_id}"
  iam_instance_profile = "${module.bastion.iam_instance_profile}"
  private_subnet_1c = "${module.network.private_subnet_1c}"
  private_subnet_1b = "${module.network.private_subnet_1b}"

  public_subnet_1c = "${module.network.public_subnet_1c}"
  public_subnet_1b = "${module.network.public_subnet_1b}"

  bastion_public_ip = "${module.bastion.bastion_public_ip}"

  rds_dns_name = "${module.db.rds_dns_name}"

  app_server_ami = "${var.app_server_ami}"

}
