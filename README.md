# Overview

Setups a simple infrastructure with a bastion host, a backend server (web and app) and
a RDS(PostgreSQL) db. 
Deploys a simple Django app on the app server using Ansible.

## Installation

	Make sure you have terraform installed. My terraform -v command has the following output

	Terraform v0.11.1
	+ provider.aws v1.7.1
	+ provider.template v1.0.0

	Your version of Terraform is out of date! The latest version
	is 0.11.2. You can update by downloading from www.terraform.io/downloads.html


## Setup Infrastructure

	git clone the repo followed by
	``` terraform init
		terraform plan -out "Name.State"
		terraform apply --auto-approve "Name.State"
	```
	Do make sure to provide all the necessary parameter values specific to your account
	in terraform.tfvars file. 

	Please add the key_pair for aws access to your ssh-agent. 
	``` ssh-add PATH_TO_AWS_KEY_PAIR_FILE ```

## Verification
	Use the DNS name of the ELB to get to the site. I dont have a registered domain hence I could not setup a route53 url for it. The django app is a crud app for books.