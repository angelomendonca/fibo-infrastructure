# Place holder file...please replace values with specific ones to your aws account


# specify the aws region
aws_region        = "########"

# key pair
# key_pair          = "ap-southeast-prague"
key_pair          = "#############"

# app server ami- can be changed with a custom ami
# Singapore
# app_server_ami    = "ami-10acfb73" 
# Sydney
app_server_ami    = "ami-37df2255"

# Nat instance ami, these have the string "amzn-ami-vpc-nat" in their name
# bastion_host_ami  = "ami-018c3062" -- Singapore
bastion_host_ami  = "ami-b6e205d4" # Sydney

database_name     = "#######"
database_user     = "#"
database_password = "#####"
access_key = "#########"
secret_key = "################"